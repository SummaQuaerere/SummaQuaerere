# SummaQuaerere

SummaQuaerere is an open-source software for building national search engines.

## Why

By creating this software we address questionable ethics of global search platforms.

## How can I contribute?

- [ ]  [Volunteer]
- [ ]  [Donate hardware]
- [ ]  [Place our banner]

## How can I launch my own search engine?

- Contact us

## Roadmap

[Roadmap 2022](https://gitlab.com/SummaQuaerere/SummaQuaerere/-/issues/1).

## License
Apache-2 license

## Project status

Project is actively developed.
