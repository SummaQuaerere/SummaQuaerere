.. SummaQuaerere documentation master file, created by
   sphinx-quickstart on Fri Jul 22 06:14:19 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Добро пожаловать в "Сумму поиска"!
=========================================

.. toctree::
   :maxdepth: 3
   :caption: Общая информация

   general/introduction
   general/codebase
   general/newsystem

.. toctree::
   :caption: Библиотеки

   libs/list

.. toctree::
   :caption: Системы

   systems/introduction

.. toctree::
   :caption: Контакты

   contacts/contacts

