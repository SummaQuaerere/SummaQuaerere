#include "freq_dist.h"

#include <bsoncxx/builder/stream/document.hpp>

#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;
using bsoncxx::builder::basic::document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::finalize;

namespace csys {

bool FreqDist::operator==(const FreqDist &_a) const {

	return m_dist == _a.m_dist;
}

std::ostream& FreqDist::operator<<(std::ostream& o) const {

	o << "[";
	for (const int & a : m_dist)
		o << a << " ";
	o << "]";
	return o;
}

bsoncxx::document::value FreqDist::toBson() const {

	auto doc = bsoncxx::builder::basic::document{}; 
	bsoncxx::builder::basic::array fd_bs;
 
	for (auto threshold : m_dist) {
		
		fd_bs.append( threshold );
	}
 	doc.append( kvp(  "fd", fd_bs ));
	doc.append( kvp(  "begin_ts", std::chrono::time_point_cast<std::chrono::milliseconds>(begin_ts).time_since_epoch().count() ));
	doc.append( kvp(  "end_ts", std::chrono::time_point_cast<std::chrono::milliseconds>(end_ts).time_since_epoch().count() ));

	return doc.extract();
}

FreqDist::FreqDist(const bsoncxx::v_noabi::document::view &_bson) {

	if ( _bson["fd"] ) {

		for (auto fd_bs : _bson["fd"].get_array().value) {

			if (fd_bs.type() == bsoncxx::type::k_int64)
				m_dist.push_back( fd_bs.get_int64() );
			else
				m_dist.push_back( fd_bs.get_int32() );
		}
	}
	if ( _bson["begin_ts"] ) {

		if (_bson["begin_ts"].type() == bsoncxx::type::k_int64)
			begin_ts = TimePoint{ std::chrono::milliseconds{ _bson["begin_ts"].get_int64() } };
		else
			begin_ts = TimePoint{ std::chrono::milliseconds{ _bson["begin_ts"].get_int32() } };
	}
	if ( _bson["end_ts"] ) {

		if (_bson["end_ts"].type() == bsoncxx::type::k_int64)
			end_ts = TimePoint{ std::chrono::milliseconds{ _bson["end_ts"].get_int64() } };
		else
			end_ts = TimePoint{ std::chrono::milliseconds{ _bson["end_ts"].get_int32() } };
	}
}

FreqDist::FreqDist(const std::vector<int> &_dist, TimePoint _begin_ts, TimePoint _end_ts):
	m_dist(_dist),
	begin_ts(_begin_ts),
	end_ts(_end_ts) {
}

}
