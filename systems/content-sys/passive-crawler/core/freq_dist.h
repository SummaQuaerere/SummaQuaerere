#ifndef CSYS_FREQ_DIST_H_
#define CSYS_FREQ_DIST_H_

#include <vector>
#include <iostream>

#include "passive-crawler/core/time_point.h"

#include <bsoncxx/builder/basic/document.hpp>

namespace csys {

class FreqDist {
public:
	std::vector< int > m_dist;
	TimePoint begin_ts;
	TimePoint end_ts;

	bool operator==(const FreqDist &_a) const;

	std::ostream& operator<<(std::ostream& o) const;

	bsoncxx::document::value toBson() const;

	FreqDist(const bsoncxx::v_noabi::document::view &_bson);
	FreqDist(const std::vector<int> &_dist, TimePoint _begin_ts, TimePoint _end_ts);
};


}

#endif
