#ifndef CSYS_LAYERS_CONFIG_H_
#define CSYS_LAYERS_CONFIG_H_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <json/json.h>

namespace csys {

struct LayersConfig {

	struct Layer {
		int priority;
		int recrawl_period;
	};

	int download_timeout;
	std::vector< std::pair<int, Layer > > layers;

	LayersConfig(const std::string &_filename) {

		std::ifstream c_stream(_filename);
		std::string c_string((std::istreambuf_iterator<char>(c_stream)), std::istreambuf_iterator<char>());
		
		Json::Value config;
		if (!Json::Reader().parse( c_string, config )) {
			throw std::invalid_argument("LayersConfig::start could not parse config");
		}

		download_timeout = config["download_timeout"].asInt();
		for (auto layer : config["layers"]) {

			auto priority = layer["priority"].asInt();
			layers.push_back(std::make_pair(priority, Layer{
				priority,
				layer["recrawl_period"].asInt()
			}));
		}
	}
};

}

#endif
