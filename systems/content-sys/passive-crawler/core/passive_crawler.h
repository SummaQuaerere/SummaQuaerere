#ifndef CSYS_PASSIVE_CRAWLER_H_
#define CSYS_PASSIVE_CRAWLER_H_

#include "passive-crawler/core/url_map.h"
#include "passive-crawler/core/url_prioritizer.h"
#include "passive-crawler/core/layers_config.h"

#include "passive-crawler/db/url_prioritizer_db.h"

namespace csys {

template<int NLAYERS, class ParserExported>
class PassiveCrawler {
public:

	typedef UrlMap<NLAYERS, ParserExported> UrlMapImpl;
	typedef UrlPrioritizer<NLAYERS, ParserExported> UrlPrioritizerImpl;
	//typedef StreamReader<ParserExported> StreamReaderImpl;

	LayersConfig(const std::string &_config_filename);

	void passiveLoop() {

		// if there logs to read?
		//	read

		// look up stats url_map

		// inc reqs_today 

		// if not needDownloading
		//	save to db
		//  skip

		// p = prioritizeUrl
		// update last_enqueued
		// enqeue

		// save to db
		// save analytics
	}

	bool needDownloading(const UrlStats &_stats) {

	}

	int prioritizeUrl(const UrlStats &_stats) {

	}

	void checkReadStream() {

	}
};

}

#endif
