#ifndef CSYS_TIME_POINT_H_
#define CSYS_TIME_POINT_H_

#include <chrono>
#include "date/date.h"

namespace csys {

typedef std::chrono::system_clock::time_point TimePoint;

}

#endif
