#ifndef CSYS_URL_MAP_H_
#define CSYS_URL_MAP_H_

#include <unordered_map>

namespace csys {

template<int NLAYERS, class ParserExported>
class UrlMap : public UrlMapDb {

	std::unordered_map< std::string, UrlStats > m_map;

public:

	UrlMapDb(
		const std::string &_exported_log_dir,
		const std::string &_conn_str,
		const std::string &_db_name,
		std::function<void(const std::string &)> _logDbErr);

	std::optional<UrlStats> getStats(const std::string &_url);


};

}

#endif
