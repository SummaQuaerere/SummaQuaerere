#ifndef CSYS_URL_NORM_H_
#define CSYS_URL_NORM_H_

#include <skyr/url.hpp>
#include <skyr/percent_encoding/percent_decode.hpp>
#include <sstream>

namespace csys {

std::string normUrl(const skyr::url &_url) {

	std::stringstream ss;
	ss << _url.protocol() + "//";
	ss << _url.hostname();
	ss << skyr::percent_decode(_url.pathname()).value();

	const auto &search = _url.search_parameters();
	if (!search.empty())
		ss << "?";

	size_t i = 0;
	for (const auto &[key, value] : search) {
		ss << key << "=" << value;
		if (i != search.size() - 1)
			ss << "&";
		++i;
	}

	return ss.str();
}

std::string normUrl(const std::string &_url) {

	skyr::url url{ _url };
	return normUrl( url );
}

}

#endif
