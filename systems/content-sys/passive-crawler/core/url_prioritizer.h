#ifndef CSYS_URL_PRIORITIZER_H_
#define CSYS_URL_PRIORITIZER_H_

#include <thread>
#include <optional>
#include <shared_mutex>
#include <mutex>

#include <date/date.h>

#include "passive-crawler/parselogs/build_url_freq_distr.h"
#include "passive-crawler/db/url_prioritizer_db.h"

namespace csys {

/*
	log_dir - input for yesterday url freq distribution calc
	url_freq_distr - save distribution and preload on start

	midnight: recalc freq distribution
	
*/

template<int NLAYERS, class ParserExported>
class UrlPrioritizer : public UrlPrioritizerDb {

	typedef std::shared_mutex Lock;
	typedef std::unique_lock< Lock > WriteLock;
	typedef std::shared_lock< Lock > ReadLock;

	typedef BuildUrlFreqDistr<NLAYERS, ParserExported> BuildUrlFreqDistrImpl;

	Lock m_lock;
	std::optional<FreqDist> m_dist;

	const std::string m_exported_log_dir;
	std::function<void(const std::string &)> m_logDbErr;

	std::thread m_thread;


	/* thread for
	 	preload distribuition from db
	 	recalc distribition
	 	dump distribution
	*/

	bool m_running { true };

public:

	UrlPrioritizer(
		const std::string &_exported_log_dir,
		const std::string &_conn_str,
		const std::string &_db_name,
		std::function<void(const std::string &)> _logDbErr):

		UrlPrioritizerDb(_conn_str, _db_name, _logDbErr),

		m_exported_log_dir(_exported_log_dir),
		m_logDbErr(_logDbErr),
		m_thread(&UrlPrioritizer::_thread, this) {
		}

	~UrlPrioritizer() {
		m_running = false;
		m_thread.join();
	}

	int getPriority(uint64_t _reqs_today) {
		// -1 if not built distribution yet

		TimePoint now = std::chrono::system_clock::now();
		TimePoint day_start = date::floor<date::days>(now);
		uint64_t extrapolated_url_reqs = _reqs_today * 86400 / std::chrono::duration_cast<std::chrono::seconds> (now - day_start).count();

		ReadLock read_lock(m_lock);

		if (!m_dist)
			return -1;

		for (size_t i = 0; i < m_dist->m_dist.size(); ++i) {

			if (extrapolated_url_reqs > m_dist->m_dist[i])
				return i;
		}
		return m_dist->m_dist.size() - 1;
	}


private:

	void _thread() {

		try {
	
			UrlPrioritizerDb::connectMg();
			_checkPreloadDist();
				
		} catch (std::exception &_ex) {
		  	
			m_logDbErr(std::string("UrlPrioritizer::_thread ") + _ex.what());
		}

		while (m_running) {

			try {
	
				_checkRebuildDist();
				
			} catch (std::exception &_ex) {
			  	
				m_logDbErr(std::string("UrlPrioritizer::_thread ") + _ex.what());
			}			

			sleep(1);
		}
	}

	void _checkPreloadDist() {

		WriteLock lock(m_lock);
		m_dist = UrlPrioritizerDb::loadDist();
	}

	void _checkRebuildDist() {

		TimePoint now = std::chrono::system_clock::now();
		auto yes_end = date::floor<date::days>(now);

		if (m_dist) {

			if (m_dist->end_ts + std::chrono::hours(1) > yes_end)
				return;
		}

		// build from log
		auto yes_start = yes_end - std::chrono::hours(24);
		auto new_dist = BuildUrlFreqDistrImpl::build(m_exported_log_dir, yes_start, yes_end);

		// update
		WriteLock lock(m_lock);
		m_dist = new_dist;
		UrlPrioritizerDb::saveDist(new_dist);
	}

};

}

#endif
