#include "url_stats.h"

#include <bsoncxx/builder/stream/document.hpp>

#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;
using bsoncxx::builder::basic::document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::finalize;

namespace csys {

bool UrlStats::operator==(const UrlStats &_a) const {

	return
		(url == _a.url) && 
		(last_crawled_ts == _a.last_crawled_ts) &&
		(reqs_today == _a.reqs_today) &&
		(last_enqueued_ts == _a.last_enqueued_ts);
}

UrlStats::UrlStats(const std::string &_url,
	TimePoint _last_crawled_ts,
	int64_t _reqs_today,
	TimePoint _last_enqueued_ts):
url(_url),
last_crawled_ts(_last_crawled_ts),
reqs_today(_reqs_today),
last_enqueued_ts(_last_enqueued_ts)
{ }

std::string UrlStats::getId() const {

	return url;
}

bsoncxx::document::value UrlStats::toBson() const {

	auto doc = bsoncxx::builder::basic::document{};
	doc.append( kvp(  "_id", url ));
	int64_t last_crawled_ts_int64 = std::chrono::time_point_cast<std::chrono::milliseconds>(last_crawled_ts).time_since_epoch().count();
	doc.append( kvp(  "last_crawled_ts", last_crawled_ts_int64 ));
	doc.append( kvp(  "reqs_today", reqs_today ));

	int64_t last_enqueued_ts_int64 = std::chrono::time_point_cast<std::chrono::milliseconds>(last_enqueued_ts).time_since_epoch().count();	
	doc.append( kvp(  "last_enqueued_ts", last_enqueued_ts_int64 ));
	return doc.extract();
}

UrlStats::UrlStats(const bsoncxx::v_noabi::document::view &_bson) {

	auto url_view = _bson["_id"].get_utf8().value;
	url = std::string ( url_view.data(), url_view.size() );

	if (_bson["last_crawled_ts"].type() == bsoncxx::type::k_int64)
	  last_crawled_ts = TimePoint(std::chrono::milliseconds{ _bson["last_crawled_ts"].get_int64() } );
	else
	  last_crawled_ts = TimePoint(std::chrono::milliseconds{ _bson["last_crawled_ts"].get_int32() } );

	if (_bson["reqs_today"].type() == bsoncxx::type::k_int64)
	  reqs_today = _bson["reqs_today"].get_int64();
	else
	  reqs_today = _bson["reqs_today"].get_int32();

	if (_bson["last_enqueued_ts"].type() == bsoncxx::type::k_int64)
	  last_enqueued_ts = TimePoint(std::chrono::milliseconds{ _bson["last_enqueued_ts"].get_int64() } );
	else
	  last_enqueued_ts = TimePoint(std::chrono::milliseconds{ _bson["last_enqueued_ts"].get_int32() } );
}
/*
std::ostream& UrlStats::operator<< (std::ostream &o) const {

	int64_t last_crawled_ts_int64 = std::chrono::time_point_cast<std::chrono::milliseconds>(last_crawled_ts).time_since_epoch().count();
	o  << "[" << url << ", " << last_crawled_ts_int64 << ", " << reqs_today << ", " << last_enqueued << " ]";
	return o;
}
*/
}
