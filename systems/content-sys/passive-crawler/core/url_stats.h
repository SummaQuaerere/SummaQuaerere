#ifndef CSYS_URL_STATS_H_
#define CSYS_URL_STATS_H_

#include "passive-crawler/core/time_point.h"
#include <string>
#include <bsoncxx/builder/basic/document.hpp>

namespace csys {

class UrlStats {
public:

	std::string url;
	TimePoint last_crawled_ts;
	int64_t reqs_today;
	TimePoint last_enqueued_ts;

	UrlStats(const std::string &_url,
		TimePoint _last_crawled_ts,
		int64_t _reqs_today,
		TimePoint _last_enqueued_ts);

	std::string getId() const;

	bool operator==(const UrlStats &_a) const;
	bsoncxx::document::value toBson() const;
	UrlStats(const bsoncxx::v_noabi::document::view &_bson);

	//std::ostream& operator<< (std::ostream &o) const;
};

}

#endif
