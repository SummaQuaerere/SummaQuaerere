#include "url_map_db.h"
#include <bsoncxx/builder/stream/document.hpp>

#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;
using bsoncxx::builder::basic::document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::finalize;

namespace csys {

UrlMapDb::UrlMapDb(
		const std::string &_conn_str,
		const std::string &_db_name,
		std::function<void(const std::string &)> _logDbErr):
	m_conn_str(_conn_str),
	m_db_name(_db_name),
	m_logDbErr(_logDbErr) {

	_connectMg();
}

void UrlMapDb::_connectMg() {

	m_conn = mongocxx::client{ mongocxx::uri{ m_conn_str } };
	m_db = m_conn[ m_db_name ];
	m_url_map_coll = m_db[ m_dist_coll_name ];
}

void UrlMapDb::insert(const UrlStats &_stats) {

	m_url_map_coll.insert_one(_stats.toBson().view());
}

void UrlMapDb::update(const UrlStats &_stats) {

	m_url_map_coll.update_one(
	  bsoncxx::builder::stream::document{} << "_id" << _stats.getId()<< finalize,
	  bsoncxx::builder::stream::document{} << "$set"
			<< _stats.toBson()
			<< finalize
	);
}

void UrlMapDb::getAll(std::vector<UrlStats> &_url_stats_all) {

	mongocxx::cursor cursor = m_url_map_coll.find({});
	for(auto doc : cursor) {
	 
		_url_stats_all.push_back( UrlStats(doc) );
	}
}

void UrlMapDb::loadMap(UrlMap &_map) {

}

}
