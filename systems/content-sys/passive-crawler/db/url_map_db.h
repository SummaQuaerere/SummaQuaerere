#ifndef CSYS_URL_MAP_DB_H_
#define CSYS_URL_MAP_DB_H_

#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/options/find.hpp>
#include <mongocxx/uri.hpp>

#include "passive-crawler/core/url_stats.h"

#include <unordered_map>
#include <queue>

namespace csys {

class UrlMapDb {

	const std::string m_dist_coll_name { "url_map" } ;

	const std::string m_conn_str;
	const std::string m_db_name;

	mongocxx::client m_conn;
	mongocxx::database m_db;
	mongocxx::collection m_url_map_coll;

	std::function<void(const std::string &)> m_logDbErr;	

	std::queue<UrlStats> m_flush_queue;

	void _connectMg();

public:

	typedef std::unordered_map< std::string, UrlStats > UrlMap;

	UrlMapDb(
		const std::string &_conn_str,
		const std::string &_db_name,
		std::function<void(const std::string &)> _logDbErr);

	void loadMap(UrlMap &_map);

	void getAll(std::vector<UrlStats> &_url_stats_all);
	void insert(const UrlStats &_stats);
	void update(const UrlStats &_stats);
};

}

#endif
