#include "url_prioritizer_db.h"

#include <bsoncxx/builder/stream/document.hpp>

#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>

using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::make_array;
using bsoncxx::builder::basic::make_document;
using bsoncxx::builder::basic::document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::finalize;

namespace csys {

UrlPrioritizerDb::UrlPrioritizerDb(
		const std::string &_conn_str,
		const std::string &_db_name,
		std::function<void(const std::string &)> _logDbErr):
	m_conn_str(_conn_str),
	m_db_name(_db_name),
	m_logDbErr(_logDbErr) {

}

std::optional<FreqDist> UrlPrioritizerDb::loadDist() {

	mongocxx::cursor cursor = m_dist_coll.find({});
	mongocxx::cursor::iterator it = cursor.begin();
	mongocxx::cursor::iterator end = cursor.end();
	
	if (it != end) {
		
		return std::optional<FreqDist>( FreqDist(*it) );
	}
	return std::optional<FreqDist>();
}

void UrlPrioritizerDb::saveDist(const FreqDist &_dist) {

	m_dist_coll.delete_many( bsoncxx::builder::stream::document{} << finalize);
	
	auto doc = _dist.toBson();	
	if (!m_dist_coll.insert_one( doc.view() )) {
		
		throw std::runtime_error("UrlPrioritizerDb::saveDist write fail");
	}
}

void UrlPrioritizerDb::connectMg() {

	m_conn = mongocxx::client{ mongocxx::uri{ m_conn_str } };
	m_db = m_conn[ m_db_name ];
	m_dist_coll = m_db[ m_dist_coll_name ];
}

}
