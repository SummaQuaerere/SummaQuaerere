#ifndef CSYS_URL_PRIORITIZER_DB_H_
#define CSYS_URL_PRIORITIZER_DB_H_

#include "passive-crawler/core/freq_dist.h"
#include <string>
#include <optional>

#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/json.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/options/find.hpp>
#include <mongocxx/uri.hpp>

namespace csys {

class UrlPrioritizerDb {

	const std::string m_dist_coll_name { "url_prioritizer_dist" } ;

	const std::string m_conn_str;
	const std::string m_db_name;

	mongocxx::client m_conn;
	mongocxx::database m_db;
	mongocxx::collection m_dist_coll;

	std::function<void(const std::string &)> m_logDbErr;

public:

	UrlPrioritizerDb(
		const std::string &_conn_str,
		const std::string &_db_name,
		std::function<void(const std::string &)> _logDbErr);

	std::optional<FreqDist> loadDist();

	void saveDist(const FreqDist &_dist);

	void connectMg();
	
private:

};

}

#endif
