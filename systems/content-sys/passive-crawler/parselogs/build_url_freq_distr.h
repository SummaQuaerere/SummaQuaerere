#ifndef PARSE_MT_PARSE_BUILD_URL_FREQ_DISTR_H_
#define PARSE_MT_PARSE_BUILD_URL_FREQ_DISTR_H_

#include "passive-crawler/parselogs/parse_interval.h"
#include "passive-crawler/core/freq_dist.h"

namespace csys {

struct url_freq_vec_comp {

	inline bool operator()(const std::pair< std::string, int > &_a, const std::pair< std::string, int > &_b) {
		return _a.second > _b.second; // descending
	}
};

template<int NLAYERS, class ParserExported>
class BuildUrlFreqDistr {

public:

	// NBUCKETS numbers for freq distr

	static FreqDist build(const std::string &_dir,
		TimePoint _ts_begin,
		TimePoint _ts_end) {

		std::map<std::string, int> url_freq;

		ParseInterval<ParserExported>(_dir, _ts_begin, _ts_end, [&](const UrlLine &_line) {
			
			++url_freq[ _line.location ];
		});

		std::vector< std::pair< std::string, int > > url_freq_vec( url_freq.begin(), url_freq.end() ); // sort
		std::sort( url_freq_vec.begin(), url_freq_vec.end(), url_freq_vec_comp() );

		/*
		for (int i = 0; i < 100; ++i) {

			std::cout << url_freq_vec[i].second << " " << url_freq_vec[i].first << std::endl;
		}*/

		// calc borders in buckets
		std::vector<int> dist;
		size_t step = url_freq_vec.size() / NLAYERS;

		for (size_t i = step; i < url_freq_vec.size(); i += step) {

			dist.push_back( url_freq_vec[i].second );
		}

		return FreqDist{dist, _ts_begin, _ts_end};
	}

};

}

#endif
