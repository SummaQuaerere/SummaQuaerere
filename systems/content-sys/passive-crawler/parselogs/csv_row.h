#ifndef _CSYS_CSVROW_H_
#define _CSYS_CSVROW_H_

#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <string_view>
#include <map>

namespace csys {

class CSVRow {
public:
    CSVRow(char _delim): m_delim(_delim) {
    }
    std::string_view operator[](std::size_t index) const {
        return std::string_view(&m_line[m_data[index] + 1], m_data[index + 1] -  (m_data[index] + 1));
    }
    std::size_t size() const {
        return m_data.size() - 1;
    }
    void readNextRow(std::istream& str) {
        std::getline(str, m_line);

        m_data.clear();
        m_data.emplace_back(-1);
        std::string::size_type pos = 0;
        while((pos = m_line.find(m_delim, pos)) != std::string::npos)
        {
            m_data.emplace_back(pos);
            ++pos;
        }
        // This checks for a trailing comma with no data after it.
        pos   = m_line.size();
        m_data.emplace_back(pos);
    }
    std::map<std::string, std::string> toMap(std::map<int, std::string> _head) const {
        std::map<std::string, std::string> ret;
        for (std::size_t i = 0; i < size(); ++i) {
            ret[ _head[i] ] = (*this)[i];
        }
        return ret;
    }
    friend std::ostream& operator<<(std::ostream &o, CSVRow& data);
private:
    char m_delim;
    std::string         m_line;
    std::vector<int>    m_data;
};

inline std::istream& operator>>(std::istream& str, CSVRow& data) {
    data.readNextRow(str);
    return str;
}

inline std::ostream& operator<<(std::ostream &o, CSVRow& data) {

    o << data.m_line;
    return o;
}

} 

#endif
