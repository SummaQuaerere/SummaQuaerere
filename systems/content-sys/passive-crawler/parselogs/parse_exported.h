#ifndef PARSE_EXPORTED_H_
#define PARSE_EXPORTED_H_

#include "passive-crawler/core/time_point.h"
#include "passive-crawler/parselogs/url_line.h"
#include "passive-crawler/parselogs/csv_row.h"
#include "passive-crawler/core/url_norm.h"

namespace csys {

class ParseExported {
public:

	struct FileInfo {
		TimePoint ts_begin;
		TimePoint ts_end;
		std::string path;
	};

	virtual int parse(const std::string &_fn, int _limit_events, std::function<void(const UrlLine&)> _onLine) = 0;

	static std::vector<std::string> split (const std::string &s, char delim) {
	    std::vector<std::string> result;
	    std::stringstream ss (s);
	    std::string item;

	    while (std::getline (ss, item, delim)) {
	        result.push_back (item);
	    }

	    return result;
	}


	static FileInfo parseFilename(const std::string &_fn) {

		// 1645109885_1645110485.tsv.gz
		auto t = split(_fn, '.');
		auto ts = split(t[0], '_');
		TimePoint ts_begin { std::chrono::seconds{ boost::lexical_cast<uint64_t>(ts[0]) } };
		TimePoint ts_end { std::chrono::seconds{ boost::lexical_cast<uint64_t>(ts[1]) } };

		return FileInfo {
				ts_begin,
				ts_end,
				_fn
		};
	}

	static std::vector<FileInfo> parseFilenames(const std::string &_dir) {

		std::vector<FileInfo> ret;
		std::set<std::filesystem::path> sorted_by_name;
		const std::filesystem::path dir{_dir};
		int dir_size = 0;
		for(auto const& dir_entry: std::filesystem::directory_iterator{dir}) {
			++dir_size;
			sorted_by_name.insert(dir_entry.path());
		}

		for (auto& dir_entry : sorted_by_name) {

			ret.push_back( parseFilename( dir_entry.filename().string() ) );
		}
		return ret;
	}

	static int findFileWithTs(const std::vector<FileInfo> &_metas, TimePoint _ts_begin) {
		
		int i = 0;
		for (const auto& _m: _metas) {
			if (_m.ts_begin >= _ts_begin && _ts_begin <= _m.ts_end)
				return i;
			++i;
		}
		return -1;
	}

	void parseDir(
		const std::string &_dir,
		int _limit_events,
		int _config_begin,
		int _config_end,
		std::function<void(const UrlLine&)> _onLine
		) {

		std::set<std::filesystem::path> sorted_by_name;

		const std::filesystem::path dir{_dir};
		int dir_size = 0;
		for(auto const& dir_entry: std::filesystem::directory_iterator{dir}) {
			++dir_size;
			sorted_by_name.insert(dir_entry.path());
		}

		// convert [-10, 0) to actual numbers

		int begin = 0;
		int end = 0;

		if (_config_begin < 0)
			begin = dir_size + _config_begin;
		else
			begin = _config_begin;

		if (begin < 0) {
			std::cout << "_config_begin too small " << _config_begin << " (" << dir_size << ")" << std::endl;
		}

		if (_config_begin >= dir_size) {
			std::cout << "_config_begin too big " << _config_begin << " (" << dir_size << ")" << std::endl;
			return;
		}

		if (_config_end == 0)
			end = dir_size;
		else if (_config_end < 0)
			end = dir_size + _config_end;
		else
			end = _config_end;

		if (end > dir_size) {
			std::cout << "_config_end too big " << _config_end << " (" << dir_size << ")" << std::endl;
		} else if (end < 0) {
			std::cout << "_config_end too small " << _config_end << " (" << dir_size << ")" << std::endl;
		}

		std::cout << "ParseExported::runOnDir dir_size: " << dir_size << std::endl;
		std::cout << "ParseExported::runOnDir running ["
			<< _config_begin << ", " << _config_end << ") -> "
			<< "[" << begin << ", " << end << ")"
			<< std::endl;

		int events = 0;
		int i = -1;
		//for(auto const& dir_entry: std::filesystem::directory_iterator{dir}) {
		for (auto& dir_entry : sorted_by_name) {

			++i;
			if (i < begin)
				continue;
			
			if (i >= end)
				break;

	        std::cout << "ParseExported::parseDir " << dir_entry << std::endl;
	        std::string fn { _dir + "/" + dir_entry.filename().string() };
	        events += parse(fn, _limit_events-events, _onLine);
		}
	}
};

}


#endif

