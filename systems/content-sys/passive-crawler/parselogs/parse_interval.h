#ifndef PARSE_PARSE_ITERATE_INTERVAL_H_
#define PARSE_PARSE_ITERATE_INTERVAL_H_

#include "passive-crawler/parselogs/mt/parse_mt_exported.h"
#include "passive-crawler/core/time_point.h"

namespace csys {

template<class ParserExported>
class ParseInterval : public ParserExported {
public:

	/*
		find start file iterating file names, skip to ts_begin
		and iterate until ts_end

		or fail find that interval
	*/

	ParseInterval(
		const std::string &_dir,
		TimePoint _ts_begin,
		TimePoint _ts_end,
		std::function<void(const UrlLine&)> _onLine) {

		auto metas = ParserExported::parseFilenames(_dir);
		int start_file = ParserExported::findFileWithTs(metas, _ts_begin);
		int end_file = ParserExported::findFileWithTs(metas, _ts_end);

		if (start_file == -1 || end_file == -1)
			throw std::invalid_argument("ParseInterval wrong range");

		ParserExported::parseDir(
			_dir,
			-1, // limit_events
			start_file,
			end_file,
			[&](const UrlLine &_line) {

				if (_ts_begin <= _line.ts && _line.ts < _ts_end) {

					_onLine(_line);
				}
			});
	}
};

}

#endif

