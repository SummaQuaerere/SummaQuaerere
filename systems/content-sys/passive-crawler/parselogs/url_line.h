#ifndef CSYS_URL_LINE_H_
#define CSYS_URL_LINE_H_

#include "passive-crawler/core/time_point.h"
#include <string>

namespace csys {

struct UrlLine {
	TimePoint ts;
	std::string domain;
	std::string location;
};

}

#endif
