PROJ_DIR=/home/phrk/searchengine/content-sys/passive-crawler
EXPORTED_LOGS_DIR=/mnt/1tb/datasets/mtlogs_exported

docker run -ti --user=root --network="host" -v $PROJ_DIR:/opt/content-sys/passive-crawler:z -v $EXPORTED_LOGS_DIR:/opt/exported_logs/:z -w /opt/content-sys/passive-crawler/tests/test_layers_config/ csysdeb bash
